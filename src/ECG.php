<?php

namespace ECG;

class ECG
{
    public $recordedAtUtc;
    public $utcOffset;
    public $jobNumber;
    public $sequenceNumber;
    public $heartRate;
    public $averageRR;
    public $device;
    public $patient;
    public $automaticInterpretation;
    public $averagesLeadSet;
    public $rhythmLeadSet;

    public function getChannelByName($name) {

        foreach($this->rhythmLeadSet->channels as $key=>$channel){
            if($channel->name == $name){
                return $channel;
            }
        }
    }

    public function recordedAtLocal(){
        return $this->recordedAtUtc->setTimezone($this->utcOffset);
    }


}