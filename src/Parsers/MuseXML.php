<?php

namespace ECG\Parsers;

use DomDocument;
use ECG\AverageRhythmLeadSet;
use ECG\Channel;
use ECG\Device;
use ECG\Gender;
use ECG\Parsers\InvalidFileTypeException;
use Carbon\Carbon;
use ECG\ECG;
use ECG\Patient;
use ECG\RhythmLeadSet;

class MuseXml implements EcgFileParser
{
    /**
     *
     * @DomDocument
     */
    protected $document;
    protected $ecgNode;

    public function __construct($file){
        $this->load($file);
    }

    public function load($file)
    {
        if(mime_content_type($file) != 'application/xml'){
            throw new InvalidFileTypeException('Invalid file type.');
        }

        $document = new DomDocument();
        $document->load($file);

        if(!$this->isMuseXml($document)){
            throw new InvalidFileTypeException('XML does not appear to be a Mortara XML');
        }

        $this->document = $document;
    }

    private function isMuseXml($document)
    {
        return $document->doctype->name == 'RestingECG' AND $document->doctype->systemId == 'restecg.dtd';
    }

    public function getECG()
    {
        $ecg = new ECG();

        $ecg->patient = new Patient($ecg);
        $ecg->patient->firstName = $this->document->getElementsByTagName('PatientFirstName')->item(0)->nodeValue;
        $ecg->patient->lastName = $this->document->getElementsByTagName('PatientLastName')->item(0)->nodeValue;
        $ecg->patient->number = $this->document->getElementsByTagName('PatientID')->item(0)->nodeValue;
        $ecg->patient->birthDate = $this->getPatientBirthDate();
        $ecg->patient->gender = new Gender($this->document->getElementsByTagName('Gender')->item(0)->nodeValue);


        $ecg->recordedAtUtc = new Carbon(
            $this->document->getElementsByTagName('AcquisitionDate')->item(0)->nodeValue . ' ' .
            $this->document->getElementsByTagName('AcquisitionTime')->item(0)->nodeValue
        );

        $ecg->heartRate = $this->document->getElementsByTagName('VentricularRate')->item(0)->nodeValue;
        $ecg->automaticInterpretation = $this->document->getElementsByTagName('Status')->item(0)->nodeValue;

/*

        $ecg->device = new Device();
        $ecg->device->manufacturer =  $this->getSourceNode()->getAttribute('MANUFACTURER');
        $ecg->device->model = $this->getSourceNode()->getAttribute('MODEL');
        $ecg->device->serialNumber = $this->getSourceNode()->getAttribute('ACQUIRING_DEVICE_SERIAL_NUMBER');
        $ecg->device->firmware = $this->getSourceNode()->getAttribute('ACQUIRING_DEVICE_SW_VERSION');
        $ecg->device->sideId = $this->document->getElementsByTagName('SITE')->item(0)->getAttribute('ID');
*/
/*
        $ecg->averagesLeadSet = new AverageRhythmLeadSet();
        $ecg->averagesLeadSet->RPeak = $this->getTypicalCycleNode()->getAttribute('R_PEAK');
        $ecg->averagesLeadSet->POnset = $this->getTypicalCycleNode()->getAttribute('P_ONSET');
        $ecg->averagesLeadSet->POffset = $this->getTypicalCycleNode()->getAttribute('P_OFFSET');
        $ecg->averagesLeadSet->QOnset = $this->getTypicalCycleNode()->getAttribute('Q_ONSET');
        $ecg->averagesLeadSet->QOffset = $this->getTypicalCycleNode()->getAttribute('Q_OFFSET');
        $ecg->averagesLeadSet->TOffset = $this->getTypicalCycleNode()->getAttribute('T_OFFSET');
        $ecg->averagesLeadSet->PDuration = $this->getTypicalCycleNode()->getAttribute('P_DURATION');
        $ecg->averagesLeadSet->PRDuration = $this->getTypicalCycleNode()->getAttribute('PR_DURATION');
        $ecg->averagesLeadSet->QRSDuration = $this->getTypicalCycleNode()->getAttribute('QRS_DURATION');
        $ecg->averagesLeadSet->QT = $this->getTypicalCycleNode()->getAttribute('QT');
        $ecg->averagesLeadSet->QTC  = $this->getTypicalCycleNode()->getAttribute('QTC');
        $ecg->averagesLeadSet->QTB  = $this->getTypicalCycleNode()->getAttribute('QTB');

        $ecg->averagesLeadSet->QTF  = $this->getTypicalCycleNode()->getAttribute('QTF')
            ? $this->getTypicalCycleNode()->getAttribute('QTF')
            : $this->getTypicalCycleNode()->getAttribute('QTCF');


        $ecg->averagesLeadSet->PAxis = $this->getTypicalCycleNode()->getAttribute('P_AXIS');
        $ecg->averagesLeadSet->QRSAxis = $this->getTypicalCycleNode()->getAttribute('QRS_AXIS');
        $ecg->averagesLeadSet->TAxis = $this->getTypicalCycleNode()->getAttribute('T_AXIS');
        $ecg->averagesLeadSet->unitsPerMV = $this->getTypicalCycleNode()->getAttribute('UNITS_PER_MV');
        $ecg->averagesLeadSet->sampleFrequency = $this->getTypicalCycleNode()->getAttribute('SAMPLE_FREQ');
        $ecg->averagesLeadSet->channels = [];
        foreach($this->getTypicalCycleChannels() as $key=>$averageChannelNode)
        {
            $ecg->averagesLeadSet->channels[] = new Channel(
                $averageChannelNode->getAttribute('NAME'),
                $this->decodeChannelData($averageChannelNode->getAttribute('DATA'))
            );
        }

        $ecg->rhythmLeadSet = new RhythmLeadSet();
        $ecg->rhythmLeadSet->unitsPerMV = $this->getChannels()->item(0)->getAttribute('UNITS_PER_MV');
        $ecg->rhythmLeadSet->sampleFrequency = $this->getChannels()->item(0)->getAttribute('SAMPLE_FREQ');
        $ecg->rhythmLeadSet->channels = [];
        foreach($this->getChannels() as $key=>$channelNode)
        {
            $ecg->rhythmLeadSet->channels[] = new Channel(
                $channelNode->getAttribute('NAME'),
                $this->decodeChannelData($channelNode->getAttribute('DATA'))
            );
        }
*/
        return $ecg;
    }


    protected function getPatientBirthDate() {
        $dateString = $this->document->getElementsByTagName('DateofBirth')->item(0)->nodeValue;;

        foreach(['d.m.Y', 'j/n/Y'] as $format) {
            try{
                $date = Carbon::createFromFormat($format, $dateString);
                return $date;
            } catch (\Exception $e) {

            }
        }

        return null;
    }


    protected function decodeChannelData($base64) {
        return unpack("s*", base64_decode($base64, true));
    }






}