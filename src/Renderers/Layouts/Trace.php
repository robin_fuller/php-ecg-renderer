<?php

namespace ECG\Renderers\Layouts;


class Trace
{
    public $lead;
    public $xOffset;
    public $yOffset;
    public $offset;
    public $length;
    public $mark;


    public function __construct($data) {

        $this->xOffset = 0;
        $this->yOffset = 0;
        $this->offset = 0;
        $this->length = 0;
        $this->mark = 0;

        foreach($data as $key=>$value){

            if(property_exists($this,$key) === false){
                continue;
            }

            $this->$key = $value;
        }
    }
}