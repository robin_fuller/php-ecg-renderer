<?php

namespace ECG;

class Gender
{
    private $raw;

    public function __construct($raw) {
        $this->raw = $raw;
    }

    public function __toString() {
        switch (strtolower(substr($this->raw, 0,1))){
            case 'm': return 'Male';
            case 'f': return 'Female';
            default: return 'Unknown';
        }
    }
}