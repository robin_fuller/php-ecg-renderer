<?php

namespace ECG;


class AverageRhythmLeadSet extends RhythmLeadSet
{
    public $RPeak;
    public $POnset;
    public $POffset;
    public $QOnset;
    public $QOffset;
    public $TOffset;
    public $PDuration;
    public $PRDuration;
    public $QRSDuration;
    public $QT;
    public $QTC;
    public $QTB;
    public $QTF;
    public $PAxis;
    public $QRSAxis;
    public $TAxis;
    public $channels;
}