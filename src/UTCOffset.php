<?php

namespace ECG;


class UTCOffset
{
    private $raw;

    public function __construct($raw) {
        $this->raw = $this->parseInput($raw);
    }

    protected function parseInput($value){
        if(preg_match('/[\-\+][0-9]{4}$/', $value)) {
            $value = substr_replace($value, ':', 3, 0);
        } else if(preg_match('/[\-\+][0-9]{4}$/', $value)) {
            $value = substr_replace($value, ':', 3, 0);
        }

        return $value;
    }

    public function __toString() {
        return $this->raw;
    }
}