<?php
/**
 * Created by PhpStorm.
 * User: robinfuller
 * Date: 13/07/16
 * Time: 12:40
 */

namespace ECG;


class Device
{
    public $manufacturer;
    public $model;
    public $firmware;
    public $serialNumber;
    public $siteId;
    public $siteName;
}