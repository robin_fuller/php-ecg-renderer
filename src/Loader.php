<?php

namespace ECG;

use ECG\Parsers\InvalidFileTypeException;

class Loader
{
    protected static $parsers = [
        '\ECG\Parsers\TechnomedJson',
        '\ECG\Parsers\MortaraXml',
        '\ECG\Parsers\MuseXml',
    ];


    public static function loadFile($file){

        foreach(static::$parsers as $parserClass){
            try{
                $parser = new $parserClass($file);
                return $parser->getECG();
            } catch (InvalidFileTypeException $exception) {
                continue;
            }

            return null;
        }
    }
}