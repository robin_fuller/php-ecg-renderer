<?php

namespace ECG\Renderers\Layouts;


class Layout
{
    public $name;
    public $heightInUnits;
    public $widthInUnits;
    public $unitsPerMillivolt;
    public $unitsPerSecond;
    public $grids;
    public $calibrators;
    public $traces;

    protected $arrayItemClasses = [
        'grids' => '\ECG\Renderers\Layouts\Grid',
        'calibrators' => '\ECG\Renderers\Layouts\Calibrator',
        'traces' => '\ECG\Renderers\Layouts\Trace'
    ];

    static public $layouts = [
        '_12x1' => '$_12x1.json',
        '_8x1' => '$_8x1.json',
        '_3x3P1' => '$_3x3P1.json',
        '_3x4P1' => '$_3x4P1.json',
    ];

    public function __construct($data) {

        foreach($data as $key=>$value){

            if(property_exists($this,$key) === false){
                continue;
            }

            if(array_key_exists($key, $this->arrayItemClasses)){
                $class = $this->arrayItemClasses[$key];
                $this->$key = array_map(function($item) use ($class, $key){
                    return new $class($item);
                }, $value);
                continue;
            }

            $this->$key = $value;
        }
    }

    public static function Load($layoutKey)
    {
        $jsonFile = str_replace('$', __DIR__ . '/', static::$layouts[$layoutKey]);
        $blueprint = json_decode(file_get_contents($jsonFile));

        return new Layout($blueprint);
    }

    public static function registerLayoutFile($key, $file) {
        static::$layouts[$key] = $file;
    }
}