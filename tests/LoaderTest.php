<?php

use PHPUnit\Framework\TestCase;

class LoaderTest  extends TestCase
{
    protected $files;

    public function __construct()
    {
        $this->files = [
            'mortara-xml' => __DIR__ . '/mortara-xml.xml',
            'technomed-json' => __DIR__ . '/technomed.json',
        ];
    }

    public function test_mortara_xml_parser_can_load_file()
    {
        new ECG\Parsers\MortaraXml($this->files['mortara-xml']);
    }

    public function test_mortara_xml_loader_can_get_ecg_test_time()
    {
        $mortaraXmlParser = new ECG\Parsers\MortaraXml($this->files['mortara-xml']);
        $ecg = $mortaraXmlParser->getECG();
    }


    public function test_technomed_json_parser_can_load_file()
    {
        new ECG\Parsers\TechnomedJson($this->files['technomed-json']);
    }



    public function test_can_load_layout()
    {
        $layout = \ECG\Renderers\Layouts\Layout::load('_3x4P1');
    }

}