<?php

namespace ECG\Renderers\PDF;


use ECG\ECG;
use ECG\Renderers\Layouts\Layout;
use fpdf\FPDF_EXTENDED;

class PDF
{
    protected $ecg;
    protected $pdf;

    protected $anonymous;

    public function __construct(ECG $ecg) {
        $this->ecg = $ecg;
        $this->pdf = new FPDF_EXTENDED();
        $this->anonymous = false;
    }

    public function addPage(Layout $layout) {
        (new Page($this->pdf, $this->ecg, $layout))->anonymise($this->anonymous)->render();
        return $this;
    }

    public function output($destination = null){
        $this->pdf->output($destination);
    }

    public function anonymise($anonymous = true)
    {
        $this->anonymous = $anonymous;
        return $this;
    }

}