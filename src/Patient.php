<?php

namespace ECG;


use Carbon\Carbon;

class Patient
{
    public $number;
    public $title;
    public $firstName;
    public $lastName;
    public $gender;
    public $birthDate;
    public $age;
    public $note;

    private $ecg;

    public function __construct(ECG $ecg) {
        $this->ecg = $ecg;
    }

    public function age(){

        if($this->age) {
            return $this->age;
        }

        if(isset($this->birthDate)) {
            return $this->birthDate->diff($this->ecg->recordedAtLocal())->format('%y');
        }

        return null;
    }
}