<?php

namespace ECG\Renderers\Layouts;


class Grid
{
    public $step;
    public $rgb;

    public function __construct($data) {

        foreach($data as $key=>$value){

            if(property_exists($this,$key) === false){
                continue;
            }

            $this->$key = $value;
        }
    }
}