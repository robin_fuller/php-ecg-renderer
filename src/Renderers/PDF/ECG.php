<?php

namespace ECG\Renderers\PDF;


use ECG\Renderers\Layouts\Calibrator;
use ECG\Renderers\Layouts\Layout;
use fpdf\FPDF_EXTENDED;

class ECG
{
    protected $pdf;
    protected $layout;
    protected $ecg;
    protected $posX;
    protected $poxY;
    protected $traceCompressionFactor = 1;


    function __construct(FPDF_EXTENDED &$pdf, \ECG\ECG $ecg, Layout $layout) {
        $this->pdf = $pdf;
        $this->ecg = $ecg;
        $this->layout = $layout;

        $this->posX = $this->pdf->GetX();
        $this->posY = $this->pdf->GetY();
    }

    public function render() {
        $this->drawGrids();

        foreach($this->layout->traces as $trace){
            $this->drawChannel($trace);
        }

        foreach($this->layout->calibrators as $calibrator){
            $this->drawCallibrationCurve($calibrator);
        }

    }


    protected function drawGrids(){
        $this->pdf->SetLineWidth(0.1);
        foreach($this->layout->grids as $grid){
            $this->pdf->SetDrawColor(
                $grid->rgb[0],
                $grid->rgb[1],
                $grid->rgb[2]
            );
            $this->drawVerticalGridLines($grid);
            $this->drawHorizontalGridLines($grid);
        }
        $this->drawGridBox();
    }

    protected function drawGridBox() {
        $grid = end($this->layout->grids);
        $this->pdf->SetDrawColor(
            $grid->rgb[0], $grid->rgb[1], $grid->rgb[2]
        );
        $this->pdf->Rect(
            $this->posX,
            $this->posY,
            $this->layout->widthInUnits,
            $this->layout->heightInUnits
        );
    }

    protected function drawVerticalGridLines($grid) {
        $totalVerticalLines = $this->layout->widthInUnits / $grid->step;
        for($i = 0; $i <= $totalVerticalLines; $i++) {
            $this->pdf->line(
                $this->posX + $i * $grid->step,
                $this->posY,
                $this->posX + $i * $grid->step,
                $this->posY + $this->layout->heightInUnits
            );
        }
    }

    protected function drawHorizontalGridLines($grid) {
        $totalHorizontalLines = $this->layout->heightInUnits / $grid->step;
        for($i = 0; $i <= $totalHorizontalLines; $i++) {
            $this->pdf->line(
                $this->posX,
                $this->posY + $i * $grid->step,
                $this->posX + $this->layout->widthInUnits,
                $this->posY + $i * $grid->step
            );
        }
    }


    protected function drawChannel(\ECG\Renderers\Layouts\Trace $trace){
        $this->pdf->SetLineWidth(0.1);
        $this->pdf->SetDrawColor(0,0,0);

        $ecgChannel = $this->ecg->getChannelByName($trace->lead);

        if($ecgChannel == null){
            return;
        }

        $rawDataSnippet = array_slice(
            $ecgChannel->data,
            $this->millisecondsToSignalLength($trace->offset),
            $this->millisecondsToSignalLength($trace->length)
        );

        $this->drawGraph(
            $rawDataSnippet,
            $trace->xOffset,
            $trace->yOffset,
            $trace->mark,
            $ecgChannel->name
        );
    }

    protected function drawCallibrationCurve(Calibrator $calibrator) {
        $signal = $calibrator->makeSignal(
            $this->ecg->rhythmLeadSet->sampleFrequency,
            $this->ecg->rhythmLeadSet->unitsPerMV,
            $this->layout->unitsPerSecond
        );
        $this->drawGraph(
            $signal,
            $calibrator->xOffset,
            $calibrator->yOffset
        );
    }

    protected function drawGraph($rawData, $xOffset, $yOffset, $markStart = false, $label = null){

        $data = [];
        foreach($rawData as $index=>$value){
            if($index % $this->traceCompressionFactor != 0) continue;
            $data[] = [
                $index / $this->ecg->rhythmLeadSet->sampleFrequency * $this->layout->unitsPerSecond,
                $this->sampleToNv($value) * $this->layout->unitsPerMillivolt  * -1
            ];
        }

        if($markStart === true) {
            $this->pdf->line(
                $this->posX + $xOffset,
                $this->posY + $yOffset - 10,
                $this->posX + $xOffset,
                $this->posY + $yOffset - 7
            );

            $this->pdf->line(
                $this->posX + $xOffset - .25,
                $this->posY + $yOffset - 7.5,
                $this->posX + $xOffset,
                $this->posY + $yOffset - 7
            );

            $this->pdf->line( $this->posX + $xOffset + .25,
                $this->posY + $yOffset - 7.5,
                $this->posX + $xOffset,
                $this->posY + $yOffset - 7
            );
        }

        if($label) {
            $this->pdf->SetFont('Arial','',9);
            $this->pdf->SetXY(
                $this->posX + $xOffset,
                $this->posY + $yOffset - 6
            );
            $this->pdf->Cell(50, 6, $label);
        }

        list($carryX, $carryY) = [null,null];

        foreach($data as $index=>$xy){
            list($x, $y) = $xy;
            if($index > 0) {
                $this->pdf->line(
                    $carryX + $this->posX + $xOffset,
                    $carryY + $this->posY + $yOffset,
                    $x + $this->posX + $xOffset,
                    $y + $this->posY + $yOffset
                );
            }
            list($carryX, $carryY) = $xy;
        }
    }


    protected function sampleToNv($sample){
        return $sample / $this->ecg->rhythmLeadSet->unitsPerMV;
    }

    protected function millimetersToMilliseconds($millimeters) {
        return ($millimeters / $this->layout->unitsPerSecond);
    }

    protected function millisecondsToSignalLength($milliseconds) {
        return $milliseconds * ($this->ecg->rhythmLeadSet->sampleFrequency / 1000);
    }

    protected function millimetersToSignalLength($millimeters) {
        return $this->millisecondsToSignalLength($this->millimetersToMilliseconds($millimeters));
    }



}