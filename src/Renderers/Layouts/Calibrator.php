<?php

namespace ECG\Renderers\Layouts;


class Calibrator
{
    public $xOffset;
    public $yOffset;

    public function __construct($data) {

        foreach($data as $key=>$value){

            if(property_exists($this,$key) === false){
                continue;
            }

            $this->$key = $value;
        }
    }


    public function makeSignal($sampleFrequency, $amplitude, $unitsPerSecond)
    {
        $samplesPerMillimeter = $sampleFrequency / $unitsPerSecond;
        $totalSamples = $samplesPerMillimeter * 7;
        $signal = [];

        for($i = 0; $i < $totalSamples; $i++) {
            $signal[] = ($i < ($samplesPerMillimeter) || ($i > $samplesPerMillimeter * 6))
                ? 0
                : $amplitude;
        }

        return $signal;
    }
}