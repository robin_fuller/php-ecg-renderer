<?php

namespace ECG\Parsers;

use DomDocument;
use ECG\AverageRhythmLeadSet;
use ECG\Channel;
use ECG\Device;
use ECG\Gender;
use ECG\Parsers\InvalidFileTypeException;
use Carbon\Carbon;
use ECG\ECG;
use ECG\Patient;
use ECG\RhythmLeadSet;
use ECG\UTCOffset;

class TechnomedJson implements EcgFileParser
{
    protected $document;

    public function __construct($file){
        $this->load($file);
    }

    public function load($file)
    {
        if(mime_content_type($file) != 'text/plain'){
            throw new InvalidFileTypeException('Invalid file type.');
        }

        if(!$document = json_decode(file_get_contents($file))){
            throw new InvalidFileTypeException('Invalid file type. - cannot decode json');
        }

        if(!isset($document->version) OR !isset($document->patient) OR !isset($document->rhythmLeadSet)){
            throw new InvalidFileTypeException('Invalid file type. - doesn\'t contain important properties');
        }

        $this->document = $document;
    }

    public function getECG()
    {
        $ecg = new ECG();
        $ecg->recordedAtUtc = new Carbon($this->document->recordedAtUtc, 'UTC');
        if($this->document->utcOffset) $ecg->utcOffset = new UTCOffset($this->document->utcOffset);
        $ecg->heartRate = $this->document->heartRate;
        $ecg->averageRR = $this->document->averageRR;
        $ecg->automaticInterpretation = $this->document->automaticInterpretation;
        $ecg->jobNumber = $this->document->jobNumber;
        $ecg->sequenceNumber = $this->document->sequenceNumber;

        $ecg->device = new Device();
        $ecg->device->manufacturer = $this->document->device->manufacturer;
        $ecg->device->model = $this->document->device->model;
        $ecg->device->serialNumber = $this->document->device->serialNumber;
        $ecg->device->firmware = $this->document->device->firmware;
        $ecg->device->siteId = $this->document->device->siteId;

        $ecg->patient = new Patient($ecg);
        $ecg->patient->firstName = $this->document->patient->firstName;
        $ecg->patient->lastName = $this->document->patient->lastName;
        $ecg->patient->number = $this->document->patient->number;
        $ecg->patient->birthDate = new Carbon($this->document->patient->birthDate);
        $ecg->patient->age = $this->document->patient->age;
        $ecg->patient->gender =  new Gender($this->document->patient->gender);
        $ecg->patient->note = $this->document->patient->note;

        $ecg->averagesLeadSet = new AverageRhythmLeadSet();

        if($this->document->averageLeadSet != null) {
            $ecg->averagesLeadSet->RPeak = $this->document->averageLeadSet->RPeak;
            $ecg->averagesLeadSet->POnset = $this->document->averageLeadSet->POnset;
            $ecg->averagesLeadSet->POffset = $this->document->averageLeadSet->POffset;
            $ecg->averagesLeadSet->QOnset = $this->document->averageLeadSet->QOnset;
            $ecg->averagesLeadSet->QOffset = $this->document->averageLeadSet->QOffset;
            $ecg->averagesLeadSet->TOffset = $this->document->averageLeadSet->TOffset;
            $ecg->averagesLeadSet->PDuration = $this->document->averageLeadSet->PDuration;
            $ecg->averagesLeadSet->PRDuration = $this->document->averageLeadSet->PRDuration;
            $ecg->averagesLeadSet->QRSDuration = $this->document->averageLeadSet->QRSDuration;
            $ecg->averagesLeadSet->QT = $this->document->averageLeadSet->QT;
            $ecg->averagesLeadSet->QTC = $this->document->averageLeadSet->QTC;
            $ecg->averagesLeadSet->QTB = $this->document->averageLeadSet->QTB;
            $ecg->averagesLeadSet->QTF = $this->document->averageLeadSet->QTF;
            $ecg->averagesLeadSet->PAxis = $this->document->averageLeadSet->PAxis;
            $ecg->averagesLeadSet->QRSAxis = $this->document->averageLeadSet->QRSAxis;
            $ecg->averagesLeadSet->TAxis = $this->document->averageLeadSet->TAxis;
            $ecg->averagesLeadSet->unitsPerMV = $this->document->averageLeadSet->unitsPerMV;
            $ecg->averagesLeadSet->sampleFrequency = $this->document->averageLeadSet->sampleFrequency;

            $ecg->averagesLeadSet->channels = [];
            foreach ($this->document->averageLeadSet->channels as $key => $averageChannel) {
                $ecg->averagesLeadSet->channels[] = new Channel(
                    $averageChannel->name,
                    $averageChannel->data
                );
            }
        }

        $ecg->rhythmLeadSet = new RhythmLeadSet();
        $ecg->rhythmLeadSet->unitsPerMV = $this->document->rhythmLeadSet->unitsPerMV;
        $ecg->rhythmLeadSet->sampleFrequency = $this->document->rhythmLeadSet->sampleFrequency;
        $ecg->rhythmLeadSet->channels = [];
        foreach($this->document->rhythmLeadSet->channels as $key=>$channel)
        {
            $ecg->rhythmLeadSet->channels[] = new Channel(
                $channel->name,
                $channel->data
            );
        }

        return $ecg;
    }






}