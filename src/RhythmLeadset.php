<?php

namespace ECG;


class RhythmLeadSet
{
    public $sampleFrequency;
    public $unitsPerMV;
    public $channels;

    public function totalChannels() {
        return count($this->channels);
    }


}