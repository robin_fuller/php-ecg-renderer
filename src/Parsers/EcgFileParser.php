<?php
/**
 * Created by PhpStorm.
 * User: robinfuller
 * Date: 13/07/16
 * Time: 14:23
 */
namespace ECG\Parsers;

interface EcgFileParser
{
    public function __construct($file);

    public function load($file);

    public function getECG();
}