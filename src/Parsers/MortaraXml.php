<?php

namespace ECG\Parsers;

use DomDocument;
use ECG\AverageRhythmLeadSet;
use ECG\Channel;
use ECG\Device;
use ECG\Gender;
use ECG\Parsers\InvalidFileTypeException;
use Carbon\Carbon;
use ECG\ECG;
use ECG\Patient;
use ECG\RhythmLeadSet;

class MortaraXml implements EcgFileParser
{
    protected $document;
    protected $ecgNode;

    public function __construct($file){
        $this->load($file);
    }

    public function load($file)
    {
        if(mime_content_type($file) != 'application/xml'){
            throw new InvalidFileTypeException('Invalid file type.');
        }

        $document = new DomDocument();
        $document->load($file);

        if(!$this->isMortaraXml($document)){
            throw new InvalidFileTypeException('XML does not appear to be a Mortara XML');
        }

        $this->document = $document;
    }

    private function isMortaraXml($document)
    {
        return $document->doctype == 'ECG' AND $document->systemId == 'mortara_1.dtd';
    }

    public function getECG()
    {
        $ecg = new ECG();
        $ecg->recordedAtUtc = new Carbon($this->getEcgNode()->getAttribute('ACQUISITION_TIME'));
        $ecg->heartRate = $this->getEcgNode()->getAttribute('VENT_RATE');
        $ecg->averageRR = $this->getEcgNode()->getAttribute('AVERAGE_RR');
        $ecg->automaticInterpretation = $this->getAutomaticInterpretationText();
        $ecg->jobNumber = $this->getDemographicById('11');
        $ecg->sequenceNumber = $this->getEcgNode()->getAttribute('SEQUENCE_NUMBER');

        $ecg->device = new Device();
        $ecg->device->manufacturer =  $this->getSourceNode()->getAttribute('MANUFACTURER');
        $ecg->device->model = $this->getSourceNode()->getAttribute('MODEL');
        $ecg->device->serialNumber = $this->getSourceNode()->getAttribute('ACQUIRING_DEVICE_SERIAL_NUMBER');
        $ecg->device->firmware = $this->getSourceNode()->getAttribute('ACQUIRING_DEVICE_SW_VERSION');
        $ecg->device->sideId = $this->document->getElementsByTagName('SITE')->item(0)->getAttribute('ID');


        $ecg->patient = new Patient($ecg);
        $ecg->patient->firstName = $this->getSubjectNode()->getAttribute('FIRST_NAME');
        $ecg->patient->lastName = $this->getSubjectNode()->getAttribute('LAST_NAME');
        $ecg->patient->number = $this->getSubjectNode()->getAttribute('ID');
        $ecg->patient->birthDate = $this->getPatientBirthDate();
        $ecg->patient->gender = new Gender($this->getSubjectNode()->getAttribute('GENDER'));

        $ecg->averagesLeadSet = new AverageRhythmLeadSet();
        $ecg->averagesLeadSet->RPeak = $this->getTypicalCycleNode()->getAttribute('R_PEAK');
        $ecg->averagesLeadSet->POnset = $this->getTypicalCycleNode()->getAttribute('P_ONSET');
        $ecg->averagesLeadSet->POffset = $this->getTypicalCycleNode()->getAttribute('P_OFFSET');
        $ecg->averagesLeadSet->QOnset = $this->getTypicalCycleNode()->getAttribute('Q_ONSET');
        $ecg->averagesLeadSet->QOffset = $this->getTypicalCycleNode()->getAttribute('Q_OFFSET');
        $ecg->averagesLeadSet->TOffset = $this->getTypicalCycleNode()->getAttribute('T_OFFSET');
        $ecg->averagesLeadSet->PDuration = $this->getTypicalCycleNode()->getAttribute('P_DURATION');
        $ecg->averagesLeadSet->PRDuration = $this->getTypicalCycleNode()->getAttribute('PR_DURATION');
        $ecg->averagesLeadSet->QRSDuration = $this->getTypicalCycleNode()->getAttribute('QRS_DURATION');
        $ecg->averagesLeadSet->QT = $this->getTypicalCycleNode()->getAttribute('QT');
        $ecg->averagesLeadSet->QTC  = $this->getTypicalCycleNode()->getAttribute('QTC');
        $ecg->averagesLeadSet->QTB  = $this->getTypicalCycleNode()->getAttribute('QTB');

        $ecg->averagesLeadSet->QTF  = $this->getTypicalCycleNode()->getAttribute('QTF')
            ? $this->getTypicalCycleNode()->getAttribute('QTF')
            : $this->getTypicalCycleNode()->getAttribute('QTCF');


        $ecg->averagesLeadSet->PAxis = $this->getTypicalCycleNode()->getAttribute('P_AXIS');
        $ecg->averagesLeadSet->QRSAxis = $this->getTypicalCycleNode()->getAttribute('QRS_AXIS');
        $ecg->averagesLeadSet->TAxis = $this->getTypicalCycleNode()->getAttribute('T_AXIS');
        $ecg->averagesLeadSet->unitsPerMV = $this->getTypicalCycleNode()->getAttribute('UNITS_PER_MV');
        $ecg->averagesLeadSet->sampleFrequency = $this->getTypicalCycleNode()->getAttribute('SAMPLE_FREQ');
        $ecg->averagesLeadSet->channels = [];
        foreach($this->getTypicalCycleChannels() as $key=>$averageChannelNode)
        {
            $ecg->averagesLeadSet->channels[] = new Channel(
                $averageChannelNode->getAttribute('NAME'),
                $this->decodeChannelData($averageChannelNode->getAttribute('DATA'))
            );
        }

        $ecg->rhythmLeadSet = new RhythmLeadSet();
        $ecg->rhythmLeadSet->unitsPerMV = $this->getChannels()->item(0)->getAttribute('UNITS_PER_MV');
        $ecg->rhythmLeadSet->sampleFrequency = $this->getChannels()->item(0)->getAttribute('SAMPLE_FREQ');
        $ecg->rhythmLeadSet->channels = [];
        foreach($this->getChannels() as $key=>$channelNode)
        {
            $ecg->rhythmLeadSet->channels[] = new Channel(
                $channelNode->getAttribute('NAME'),
                $this->decodeChannelData($channelNode->getAttribute('DATA'))
            );
        }

        return $ecg;
    }


    protected function getEcgNode(){
        return $this->document->getElementsByTagName('ECG')->item(0);
    }

    protected function getSourceNode(){
        return $this->document->getElementsByTagName('SOURCE')->item(0);
    }

    protected function getSubjectNode(){
        return $this->document->getElementsByTagName('SUBJECT')->item(0);
    }

    protected function getTypicalCycleNode(){
        return $this->document->getElementsByTagName('TYPICAL_CYCLE')->item(0);
    }

    protected  function getTypicalCycleChannels(){
        return $this->document->getElementsByTagName('TYPICAL_CYCLE_CHANNEL');
    }

    protected  function getChannels(){
        return $this->document->getElementsByTagName('CHANNEL');
    }

    protected function getDemographicById($id) {
        $demographicElements = $this->document->getElementsByTagName('DEMOGRAPHIC_FIELD');
        foreach($demographicElements as $demographicElement) {
            if($demographicElement->getAttribute('ID') == $id){
                return $demographicElement->getAttribute('VALUE');
            }
        }
        return null;
    }


    protected function decodeChannelData($base64) {
        return unpack("s*", base64_decode($base64, true));
    }

    protected function getAutomaticInterpretationText() {
        $statementNodes = iterator_to_array($this->document->getElementsByTagName('STATEMENT'));

        return array_reduce($statementNodes, function($carry = '', $node){
            return $carry . $node->getAttribute('TEXT') . "\n";
        });
    }

    protected function getPatientBirthDate() {

        $dateString = $this->getDemographicById(16);

        foreach(['d.m.Y', 'n/j/Y'] as $format) {
            try{
                $date = Carbon::createFromFormat($format, $dateString);
                return $date;
            } catch (\Exception $e) {

            }
        }

        return null;
    }



}